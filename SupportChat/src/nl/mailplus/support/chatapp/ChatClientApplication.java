package nl.mailplus.support.chatapp;

import nl.mailplus.support.chatapp.client.business.ChatClient;
/**
 * This class contains the main method for the client applicaton
 * @author Ralph van Delft
 *
 */
public class ChatClientApplication {
	/**
	 * The main method makes a reference to the ChatClient to start up the application
	 * @param args
	 */
	public static void main(String[] args) {
		ChatClient chatClient = new ChatClient();

	}

}
