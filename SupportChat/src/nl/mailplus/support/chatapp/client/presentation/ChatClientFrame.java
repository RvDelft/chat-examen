package nl.mailplus.support.chatapp.client.presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import nl.mailplus.support.chatapp.client.business.ChatClient;

/**
 * This class shows the received messages and online clients.
 * Also gives the user an option to send a line of text.
 * @author Ralph van Delft
 *
 */
public class ChatClientFrame extends JFrame{


	private static final long serialVersionUID = -2318848536192943870L;
	private final static int _HEIGHT = 500;
	private final static int _WIDTH = 500;

	private JTextField inputTextField;
	private ChatClient chatClient;
	private JTextArea chatArea;	
	private DefaultListModel<String> onlineCLientsList;
	
	/**
	 * Constructor which sets up all the GUI for the Client Chat Application
	 * @param chatClient
	 */
	public ChatClientFrame(ChatClient chatClient, String userName){
		this.chatClient = chatClient;
		this.onlineCLientsList = new DefaultListModel<String>();
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new BorderLayout());
		this.chatArea = new JTextArea();
		this.chatArea.setLineWrap(true);
		JScrollPane chatAreaScroll = new JScrollPane(chatArea);

		this.inputTextField = new JTextField(35);
		
		SendMessageHandler sendMessageHandler = new SendMessageHandler();
		this.inputTextField.addActionListener(sendMessageHandler);
		JButton submitButton = new JButton("Send");
		submitButton.addActionListener(sendMessageHandler);

		JList<String> clientList = new JList<String>(onlineCLientsList);
		clientList.setSize((_WIDTH/10), _HEIGHT);
		
		inputPanel.setBorder(new EmptyBorder(5,5,5,5));
		inputPanel.add(inputTextField, BorderLayout.CENTER);
		inputPanel.add(submitButton, BorderLayout.EAST);
		
		mainPanel.setBorder(new EmptyBorder(10,10,10,10));
		mainPanel.add(new JScrollPane(clientList), BorderLayout.EAST);
		mainPanel.add(chatAreaScroll, BorderLayout.CENTER);
		mainPanel.add(inputPanel, BorderLayout.SOUTH);

		
		this.add(mainPanel);
		this.setSize(_WIDTH, _HEIGHT);
		this.setTitle("Support Chat -" + userName + "-");
		this.setVisible(true);
		this.addWindowListener(new WindowHandler());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.repaintFrame();
	}
	
	/**
	 * when calling this method, the whole JFrame is repainted and revalidated
	 */
	private void repaintFrame(){
		this.repaint();
		this.revalidate();
	}
	 
	/**
	 * This method will ad a new line of text to the chatArea.
	 * @param String message
	 */
	public void addNewIncommingMessage(String message){
		this.chatArea.append(message + "\n");
		this.chatArea.setCaretPosition(chatArea.getDocument().getLength());
		this.repaintFrame();
	
	}
	/**
	 * This method adds a new client to the online clientlist
	 * @param String newClient
	 */
	public void addNewOnlineClient(String newClient){
		this.onlineCLientsList.addElement(newClient);
		this.repaintFrame();
	}
	/**
	 * This method will delete the list option which has the same value of the received String.
	 * @param String client
	 */
	public void removeOnlineClient(String client){
		this.onlineCLientsList.removeElement(client);
		this.repaintFrame();
	}
	
	/**
	 * This Handler class is used by the send button. When pressing the send button this class will provide a ActionListener for the button.
	 * @author Ralph van Delft
	 *
	 */
	private class SendMessageHandler implements ActionListener{
		
		/**
		 * The required method for an ActionListener.
		 * This method will send the typed in message to the business layer and resets the textField.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			String message = inputTextField.getText();
			chatClient.newOutgoingMessage(message);
			inputTextField.setText("");
			
		}
		
	}
	
	/**
	 * This Handler class is used when an user is doing something with the frame.
	 * @version 1.0 is only listening to the closing trigger
	 * @author Ralph van Delft
	 *
	 */
	private class WindowHandler implements WindowListener{

		/**
		 * When the frame is clossing it will trigger a method in the business layer 
		 * to inform the server that this client is no longer online.
		 */
		@Override
		public void windowClosing(WindowEvent e) {
			chatClient.goOffline();
		}
		
		@Override
		public void windowClosed(WindowEvent e) {}
		
		@Override
		public void windowOpened(WindowEvent e) {}

		@Override
		public void windowIconified(WindowEvent e) {}

		@Override
		public void windowDeiconified(WindowEvent e) {}

		@Override
		public void windowActivated(WindowEvent e) {}

		@Override
		public void windowDeactivated(WindowEvent e) {}

		
	}
}
