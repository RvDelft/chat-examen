package nl.mailplus.support.chatapp.client.presentation;


import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * This class provides a information frame to show messages to the client.
 * @author Ralph van Delft
 *
 */
public class ChatClientPopupFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	private int width = 250;
	private int height = 75;
	
	/**
	 * instantiates the JFrame
	 */
	public ChatClientPopupFrame(){
		this.setDefaultCloseOperation(0);
		this.setSize(width, height);
		
	}
	/**
	 * When calling this method it will ask the user for his name and will return this String
	 * @return String username
	 */
	public String getUserName(){
		String userName =  JOptionPane.showInputDialog(this, "What is your name", "Information required", JOptionPane.INFORMATION_MESSAGE);
		return userName;
	}
	/**
	 * This method is used when the user needs to be informed. 
	 * An message will be shown in a popup
	 * @param String message
	 */
	public void showMessage(String message){
		JOptionPane.showMessageDialog(this, message, "Connecting", JOptionPane.INFORMATION_MESSAGE);
	}

	
}
