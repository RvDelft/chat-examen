package nl.mailplus.support.chatapp.client.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import nl.mailplus.support.chatapp.client.business.ChatClient;
import nl.mailplus.support.chatapp.client.business.ChatClientPopup;
import nl.mailplus.support.chatapp.models.Envelope;

/**
 * This class is the bridge between the Socket and the business layer. 
 * There is a Thread which it waiting for a Envelope Object to pass through
 * @author Ralph van Delft
 *
 */
public class ChatClientSocket {
	private ChatClient chatClient;
	private ChatClientPopup chatPopup;
	private Socket socket;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;

    /**
	 * 
	 * @param chatClient
	 */
	public ChatClientSocket(ChatClient chatClient) {
		chatPopup = new ChatClientPopup();
		this.chatClient = chatClient;

		if (socket == null) {
			this.searchForConnection();
		}
		

		try {
			this.outputStream = new ObjectOutputStream(socket.getOutputStream());
			this.inputStream = new ObjectInputStream(socket.getInputStream());
		} catch (Exception e) {
			System.err.println("Could not set op the Input or Output Stream");
		}

		Thread messageThread = new Thread(new IncommingEnvelopeHandler());
		messageThread.start();
	}

	/**
	 * This method will search's for a socket connection and will show a connection
	 * message while trying to connect
	 */
	private void searchForConnection() {
		while (true) {

			try {
				socket = new Socket("127.0.0.1", 7000);
				//connection made
				break;
			} catch (Exception e) {
				chatPopup.showMessage("Establing connection");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					System.err.println("Could not sleep for a second!");
				}

				System.err.println("Connection not established");
				continue; // continue looping if the connection failed
			}
		}
	}

	/**
	 * This method will sends a Envelope object through the outputStream
	 * @param Envelope envelope
	 */
	public void sendEnvelope(Envelope envelope) {
		try {
			outputStream.reset();
			outputStream.writeObject(envelope);
			outputStream.flush();
		} catch (Exception e) {
			System.err.println("Could nog send the Envelope message to the Server");
		}
	}
	/**
	 * This Thread class will listen to incomming Envelop objects and will send this object to the business layer.
	 * @author ralphvandelft
	 *
	 */
	public class IncommingEnvelopeHandler implements Runnable {
		Envelope newEnvelope;

		@Override
		public void run() {

			try {
				while ((this.newEnvelope = (Envelope) inputStream.readObject()) != null) {
					chatClient.openEnvelope(newEnvelope);
				}
			} catch (Exception e) {
				// Server down
				System.err.println("Server connection Lost");
				searchForConnection();
			}
		}

	}
}
