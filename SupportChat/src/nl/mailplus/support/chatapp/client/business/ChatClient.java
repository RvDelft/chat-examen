package nl.mailplus.support.chatapp.client.business;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import nl.mailplus.support.chatapp.client.presentation.ChatClientFrame;
import nl.mailplus.support.chatapp.client.service.ChatClientSocket;
import nl.mailplus.support.chatapp.models.Client;
import nl.mailplus.support.chatapp.models.Envelope;
import nl.mailplus.support.chatapp.models.Message;


/**
 * The ChatClient is responsible for processing all the new typed in messages and new incomming messages
 * Also it will ask for a username and sends this to the socket so other clients will know there is a new user (client) online
 * @author Ralph van Delft
 * @version 1.0
 *
 */
public class ChatClient {


	private ChatClientFrame chatFrame;
	private ChatClientSocket chatSocket;
	private ChatClientPopup chatPopup;
	private Client client;
	private List<Client> onlineClientList;
	
	private static final SimpleDateFormat dateTimestampFormat = new SimpleDateFormat("HH:mm:ss");
	
	/**
	 * Starts up the whole client side
	 * It makes a reference to ChatPopup,Client,ChatClientFrame and ChatClientSocket
	 * It makes a ArrayList reference for the ChatClientSocket
	 * The to set the name of the user it will ask for a String from the chatPopup with getUserName
	 * 
	 * After all this it will add the new client Object to a Envelope and gives it to sendEnvelope
	 * 
	 */
	public ChatClient(){
		this.chatPopup = new ChatClientPopup();
		String userName = chatPopup.getUserName();
		
		onlineClientList = new ArrayList<Client>();
		this.client = new Client(userName);
		this.chatFrame = new ChatClientFrame(this, userName);
		this.chatSocket = new ChatClientSocket(this);
		
		Envelope envelope = this.makeEnvelope(client);
		this.sendEnvelope(envelope);
	}
	
	/**
	 * Adds the client to a new Envelope object
	 * @param client
	 * @return Envelope with a client object
	 */
	private Envelope makeEnvelope(Client client){
		return new Envelope(client);
	}
	/**
	 * Adds the message to a new Envelope object
	 * @param message
	 * @return Envelope with a message object
	 */
	private Envelope makeEnvelope(Message message){
		return new Envelope(message);
	}
	/**
	 * Adds the received client to the onlineClientList 
	 * After that it gives the presentation layer a String with the name of the received client
	 * @param onlineClient
	 */
	private void addOnlineClient(Client onlineClient) {
		this.onlineClientList.add(onlineClient);
		this.chatFrame.addNewOnlineClient(onlineClient.toString());
	}
	
	/**
	 * Removes the received client to the onlineClientList 
	 * After that it gives the presentation layer a String with the name of the received client to remove this item
	 * @param onlineClient
	 */
	private void removeOnlineClient(Client client) {
		this.onlineClientList.remove(client);
		this.chatFrame.removeOnlineClient(client.toString());
	}


	/**
	 * Adds the username of the user to a typed in message. 
	 * Output example: 'USERNAME: text'
	 * @param String messageString
	 * @return String: 'USERNAME: text'
	 */
	private String addUserName(String messageString){
		String userNameAddedMessage = this.client.toString() + ": " + messageString;
		return userNameAddedMessage;
	}
	
	/**
	 * Adds a timestamp to the received message
	 * @param String message
	 * @return String: '12.20 USERNAME: text'
	 */
	private String addTimeStamp(String message) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String addTimeStampMessage = dateTimestampFormat.format(timestamp) + "  " + message;
		return addTimeStampMessage;
	}
	/**
	 * Unpacks the Message object and gives it to the presentation layer
	 * @param message
	 */
	private void newIncommingMessage(Message message){
		String messageString = message.toString();
		String messageWithTimeStamp = this.addTimeStamp(messageString);
		this.chatFrame.addNewIncommingMessage(messageWithTimeStamp);
	}
	/**
	 * Receives a Envelope and gives this to the Service layer to send this Envelope
	 * @param envelope
	 */
	private void sendEnvelope(Envelope envelope){
		chatSocket.sendEnvelope(envelope);
	}
	
	/**
	 * Adds the name of the user to the new message.
	 * Adds the new received message, from the presentation layer, to a new Message object 
	 * With makeEnvelope it will make a new Envelope object with this Message 
	 * After that it gives this Envelope to the sendEnvelope method
	 * @param messageString
	 */
	public void newOutgoingMessage(String messageString){
		String changedMessage = this.addUserName(messageString);
		Message message = new Message(changedMessage);
		this.sendEnvelope(makeEnvelope(message));
	}

	/**
	 * Determines if there is a Client or a Message in the Envelope object. 
	 * If it is a Message it will give it to the presentation layer 
	 * If it is a Client it will adds it to the clientList and will give the name of the new Client to the presentation layer.
	 * @param envelope
	 */
	public void openEnvelope(Envelope envelope){
		if(envelope.getMessage() != null){
			this.newIncommingMessage(envelope.getMessage());
		}else if((envelope.getClient()) != null ){
			if(envelope.getClient().isOnline()){
				this.addOnlineClient(envelope.getClient());
			}else{
				this.removeOnlineClient(envelope.getClient());
			}
		}else{
			this.chatPopup.showMessage("Received wrong object.");
		}
		
	}
	

	/**
	 * This method is called when the client frame is closed. 
	 * It sets the Client object 'online' boolean to false.
	 * And sends this Client object to the service layer.
	 */
	public void goOffline(){
		this.client.goOffline();
		this.sendEnvelope(this.makeEnvelope(this.client));
	}
}
