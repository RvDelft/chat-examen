package nl.mailplus.support.chatapp.client.business;

import nl.mailplus.support.chatapp.client.presentation.ChatClientPopupFrame;

/**
 * This class contains the business logic of the popup.
 * @author Ralph van Delft
 *
 */
public class  ChatClientPopup {
	private ChatClientPopupFrame chatPopupFrame;
	
	/**
	 * The constructor makes a reference to the presentation layer with the ChatClientPopupFrame
	 */
	public ChatClientPopup(){
		chatPopupFrame = new ChatClientPopupFrame();
	}
	
	/**
	 * This method tells the presentation layer to show this String.
	 * @param message
	 */
	public void showMessage(String message){
		chatPopupFrame.showMessage(message);
	}
	
	/**
	 * This method asks the presentation layer what the wanted username is of the user.
	 * And returns this toe the ChatClient
	 * @return String username
	 */
	public String getUserName(){
		String newName = null;
		while(newName == null || newName.isEmpty()){
			newName = this.chatPopupFrame.getUserName();
		}
		return newName;
	}
	
	/**
	 * This method is used to close the popup when it is not needed anymore
	 * 
	 * This method is not used at this moment.
	 */
	public void closeMessage(){}
}
