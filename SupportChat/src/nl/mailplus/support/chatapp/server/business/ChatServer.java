package nl.mailplus.support.chatapp.server.business;


import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import nl.mailplus.support.chatapp.models.Client;
import nl.mailplus.support.chatapp.models.Envelope;
import nl.mailplus.support.chatapp.server.presentation.ChatServerFrame;
import nl.mailplus.support.chatapp.server.service.ChatServerSocket;

/**
 * This class is the brain of the server. It keeps a list of all the online Client and their streams.
 * @author Ralph van Delft
 *
 */
public class ChatServer {
	private ChatServerSocket chatServerSocket;
	private ChatServerFrame chatServerFrame;
	
	private List<ObjectOutputStream> clientOutputStreamList;
	private List<Client> onlineClientList;
	
	/**
	 * Initializes the server.
	 * After making a reference to the ServerSocket it gives the command to wait for a new Client.
	 */
	public ChatServer(){
		this.clientOutputStreamList = new ArrayList<ObjectOutputStream>();
		this.onlineClientList = new ArrayList<Client>();
		this.chatServerFrame = new ChatServerFrame();
		this.chatServerSocket = new ChatServerSocket(this);
		this.chatServerSocket.waitForClients();

	}
	
	/**
	 * Adds the new clientStream to the clientOutputList
	 * and sends all the current online clients to this new client
	 * This last process is done before the notification to all other clients that there is a new client.
	 * So the new clients does not receive twice his own object
	 * @param client
	 */
	public void addNewClientStream(ObjectOutputStream client){
		this.clientOutputStreamList.add(client);
		this.sendNewClientOnlineClientsList(client);
	}
	/**
	 * removes the clientStream from the clientOutputList
	 * @param client
	 */
	public void removeClientStream(ObjectOutputStream client){
		this.clientOutputStreamList.remove(client);
	}
	
	/**
	 * Adds the new received client to the onlineClientList and gives the total length of the online list to the presentation layer.
	 * @param client
	 */
	private void addOnlineClient(Client client) {
		this.onlineClientList.add(client);	
		this.chatServerFrame.setTotalClientsOnline(this.onlineClientList.size());
	}
	
	/**
	 * Finds the the right client with the same name and removes this Client object from the onlineClientList.
	 * After finding one client with the same name it breaks the loop so when there are 2 or more clients with the same name only one will be deleted
	 * @param client
	 */
	private void removeOnlineClient(Client client) {
		for(Client onlineClient : this.onlineClientList){
			if(onlineClient.toString().equals(client.toString())){
				this.onlineClientList.remove(onlineClient);
				break;
			}
		}
		this.chatServerFrame.setTotalClientsOnline(this.onlineClientList.size());
	}
	/**
	 * Loops through all the Client Streams and gives the command to the ServerSocket to send this object to all of them.
	 * @param envelope
	 */
	public void sendEnvelope(Envelope envelope){
		for(ObjectOutputStream client : this.clientOutputStreamList){
			try{
				chatServerSocket.sendEnvelope(client, envelope);	
			}catch(Exception e){
				e.printStackTrace();
			}
		}

	}
	
	/**
	 * This method will sends all the online clients to the new online client.
	 * @param ObjectOutputStream newClient
	 */
	public void sendNewClientOnlineClientsList(ObjectOutputStream newClient) {
		for(Client client : this.onlineClientList){
			try{
				Envelope envelope = new Envelope(client);
				chatServerSocket.sendEnvelope(newClient, envelope);	
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * Determines if there is a Client or a Message in the Envelope object. 
	 * If it is a Message it will gives the Envelope to sendEnvelope
	 * If it is a Client it will unwrap the Envelope and gives the Client to addOnlineClient
	 * After that it will sends the new Client to all Clients to notify them.
	 * @param newEnvelope
	 */
	public void openEnvelope(Envelope envelope) {
		if(envelope.getMessage() != null){
			this.sendEnvelope(envelope);
		}else if(envelope.getClient() != null){
			if(envelope.getClient().isOnline()){
				this.addOnlineClient(envelope.getClient());
			}else{
				this.removeOnlineClient(envelope.getClient());
			}
			this.sendEnvelope(envelope);

		}
	}
}
