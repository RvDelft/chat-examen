package nl.mailplus.support.chatapp.server.presentation;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class shows how much online client there are.
 * @author Ralph van Delft
 *
 */
public class ChatServerFrame extends JFrame {
	private final static int _HEIGHT = 50;
	private final static int _WIDTH = 250;
	private JLabel textField;
	
	/**
	 * Initializes the class.
	 * Default it shows "There are 0 clients online"
	 */
	public ChatServerFrame(){
		textField = new JLabel();
		textField.setText("There are 0 clients online");
		
		
		JPanel mainPanel = new JPanel();
		mainPanel.add(textField);
		
		this.add(mainPanel);
		this.setSize(_WIDTH, _HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setTotalClientsOnline(0);
	}
	
	/**
	 * This method set's the new number of online clients.
	 * @param int total
	 */
	public void setTotalClientsOnline(int total){
		textField.setText("There are "+ total + " clients online");
		this.repaint();
		this.revalidate();
	}
}
