package nl.mailplus.support.chatapp.server.service;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import nl.mailplus.support.chatapp.models.Envelope;
import nl.mailplus.support.chatapp.server.business.ChatServer;

/**
 * This multi-Thread class is the bridge between the Socket and the business layer.
 * It has a Thread that listens for new Envelope objects which are set by the clients
 * @author Ralph van Delft
 *
 */
public class ChatServerSocket {
	private ChatServer chatServer;
	private ServerSocket serverSocket;

	/**
	 * Initializes the class
	 * @param chatServer
	 */
	public ChatServerSocket(ChatServer chatServer) {
		this.chatServer = chatServer;
		try{
			serverSocket = new ServerSocket(7000);
		}catch(Exception e){
			System.err.println("The server could not bind the 7000 port");
		}
	}
	/**
	 * This class has a loop which waits for a new Client connections.
	 */
	public void waitForClients() {
		try {
			while (true) {
				Socket clientSocket = serverSocket.accept();

				ObjectOutputStream outputStreamClient = new ObjectOutputStream(clientSocket.getOutputStream());
				this.chatServer.addNewClientStream(outputStreamClient);

				Thread envelopeThread = new Thread(new IncommingEnvelopeHandler(clientSocket, outputStreamClient));
				envelopeThread.start();

			}
		} catch (Exception e) {
			System.err.println("The server could not accept a new client or set a new outputStream");
		}
	}

	/**
	 * This class send the Envelope object through the ObjectOutputStream
	 * @param ObjectOutputStream client
	 * @param Envelope envelope
	 */
	public void sendEnvelope(ObjectOutputStream client, Envelope envelope) {
		try {
			client.reset();
			client.writeObject(envelope);
			client.flush();
		} catch (IOException e) {
			System.err.println("Object could not be send out.");
		}
	}

	/**
	 * This Thread class listens to a new Envelope object send through the InputStream.
	 * When it receives a Exeption of a Client it closes the Streams.
	 * @author Ralph van Delft
	 *
	 */
	public class IncommingEnvelopeHandler implements Runnable {
		private Envelope newEnvelope;
		private ObjectInputStream inputStream;
		private ObjectOutputStream outputStream;
		private Socket clientSocket;

		/**
		 * Initializes the new Thread for the client.
		 */
		public IncommingEnvelopeHandler(Socket clientSocket, ObjectOutputStream outputStream) {
			try {
				this.outputStream = outputStream;
				this.clientSocket = clientSocket;
				this.inputStream = new ObjectInputStream(clientSocket.getInputStream());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/**
		 * Waits for a new Envelope Object 
		 * It closes the streams when it receives a Exception.
		 */
		@Override
		public void run() {
			try {
				while ((newEnvelope = (Envelope) inputStream.readObject()) != null) {
					chatServer.openEnvelope(newEnvelope);
					newEnvelope = null;
				}
			} catch (Exception e) {
				try {
					chatServer.removeClientStream(this.outputStream);
					this.outputStream.close();
					this.inputStream.close();
					clientSocket.close();
					System.out.println("Client: " + clientSocket.toString() + " logged off");
				} catch (Exception e2) {
					System.err.println("Steams could not be closed");
				}
			}
		}
	}
}
