package nl.mailplus.support.chatapp.models;

import java.io.Serializable;

/**
 * This class will hold a String message
 * @author Ralph van Delft
 */
public class Message implements Serializable{
 
	private static final long serialVersionUID = -5169477593591668376L;
	private String message;
	
	/**
	 * Initializes the class
	 * @param message
	 */
	public Message(String message){
		this.message = message;
	}
	
	/**
	 * When calling this method it will return a String with de message in this class
	 */
	@Override
	public String toString() {
		return message;
	}
}
