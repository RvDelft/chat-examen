package nl.mailplus.support.chatapp.models;

import java.io.Serializable;

/**
 * This class will holds a Client or a Message.
 * This class is uses to send one type over the Internet connection
 * @author Ralph van Delft
 *
 */
public class Envelope implements Serializable{


	private static final long serialVersionUID = 6377526255161521480L;
	private Client client;
	private Message message;
	
	/**
	 * Initializes the class with a Client object
	 * @param Client client
	 */
	public Envelope(Client client){
		this.client = client;
	}
	/**
	 * Initializes the class with a Message object
	 * @param Message message
	 */
	public Envelope(Message message){
		this.message = message;
	}
	/**
	 * Returns a Message object.
	 * @return Message message
	 */
	public Message getMessage() {
		return message;
	}
	
	/**
	 * Returns a Client object.
	 * @return Client client
	 */	
	public Client getClient() {
		return client;
	}
}
