package nl.mailplus.support.chatapp.models;

import java.io.Serializable;

/**
 * This class holds all the information about the client.
 * @author Ralph van Delft
 *
 */
public class Client implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6315812852059784119L;
	private String name;
	private boolean online;
	

	/**
	 * Initializes the class.
	 * @param name
	 */
	public Client(String name){
		this.name = name;
		this.online= true;
		
	}
	/**
	 * Set's online to false So the server will know this client is going offline
	 */
	public void goOffline(){
		this.online = false;
	}
	/**
	 * Returns the online value
	 * @return boolean online
	 */
	public boolean isOnline(){
		return this.online;
	}
	
	/**
	 * When calling this method it will show the name of the client.
	 */
	@Override
	public String toString(){
		return this.name;
	}
}
