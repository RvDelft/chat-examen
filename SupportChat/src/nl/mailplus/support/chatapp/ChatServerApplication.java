package nl.mailplus.support.chatapp;

import nl.mailplus.support.chatapp.server.business.ChatServer;
/**
 * This class contains the main method for the server application.
 * @author Ralph van Delft
 *
 */
public class ChatServerApplication {
	/**
	 * This main method makes a reference to the ChatServer to start up the server.
	 * @param args
	 */
	public static void main(String[] args){
		ChatServer chatServer = new ChatServer();
	}
}
